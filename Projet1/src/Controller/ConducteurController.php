<?php

namespace App\Controller;

use App\Entity\conducteur;
use App\Form\ConducteurType;
use App\Repository\ConducteurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ConducteurController extends AbstractController
{
    /**
     * @Route("/Conducteur", name="Conducteur")
     */
    public function index(): Response
    {
        return $this->render('Conducteur/index.html.twig', [
            'controller_name' => 'ConducteurController',
        ]);
    }
    /**
     * @param ConducteurRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/AfficheRec",name="afficheConducteur")
     */
    public function Affiche(ConducteurRepository $repository)
    {
        $avis=$this->getDoctrine()->getManager()->getRepository(avis::class)->findAll();
        $Conducteur=$repository->findAll();
        return $this->render('afficheConducteur.html.twig',
            ['Conducteur'=>$Conducteur,'avis'=>$avis]);
    }

    /**
     * @Route("/SupprimerConducteur/{id}",name="deleteConducteur")
     */
    function Delete($id,ConducteurRepository $repository)
    {
        $Conducteur=$repository->find($id);
        $em=$this->getDoctrine()->getManager();
        $em->remove($Conducteur);
        $em->flush();
        return $this->redirectToRoute('afficheConducteur');
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/ajouterConducteur",name="ajouterConducteur")
     */
    function Add(Request $request)
    {
        $Conducteur=new conducteur();
        $form=$this->createForm(ConducteurType::class, $Conducteur);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($Conducteur);
            $em->flush();
            return $this->redirectToRoute('base');
        }
        return $this->render('ajouterConducteur.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }


    /**
     * @param Request $request
     * @Route("/ModifierConducteur/{id}",name="modifierConducteur")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    function modifier(ConducteurRepository $repository,$id,Request $request)
    {
        $Conducteur=$repository->find($id);
        $form=$this->createForm(ConducteurType::class,$Conducteur);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('afficheConducteur');
        }
        return $this->render('ajoutConducteur.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }
}
