<?php

namespace App\Controller;

use App\Entity\grappe;
use App\Form\GrappeType;
use App\Repository\GrappeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GrappeController extends AbstractController
{
    /**
     * @Route("/Grappe", name="Grappe")
     */
    public function index(): Response
    {
        return $this->render('Grappe/index.html.twig', [
            'controller_name' => 'GrappeController',
        ]);
    }
    /**
     * @param GrappeRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/AfficheRec",name="afficheGrappe")
     */
    public function Affiche(GrappeRepository $repository)
    {
        $avis=$this->getDoctrine()->getManager()->getRepository(avis::class)->findAll();
        $Grappe=$repository->findAll();
        return $this->render('afficheGrappe.html.twig',
            ['Grappe'=>$Grappe,'avis'=>$avis]);
    }

    /**
     * @Route("/SupprimerGrappe/{id}",name="deleteGrappe")
     */
    function Delete($id,GrappeRepository $repository)
    {
        $Grappe=$repository->find($id);
        $em=$this->getDoctrine()->getManager();
        $em->remove($Grappe);
        $em->flush();
        return $this->redirectToRoute('afficheGrappe');
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/ajouterGrappe",name="ajouterGrappe")
     */
    function Add(Request $request)
    {
        $Grappe=new grappe();
        $form=$this->createForm(GrappeType::class, $Grappe);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($Grappe);
            $em->flush();
            return $this->redirectToRoute('base');
        }
        return $this->render('ajouterGrappe.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }


    /**
     * @param Request $request
     * @Route("/ModifierGrappe/{id}",name="modifierGrappe")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    function modifier(GrappeRepository $repository,$id,Request $request)
    {
        $Grappe=$repository->find($id);
        $form=$this->createForm(GrappeType::class,$Grappe);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('afficheGrappe');
        }
        return $this->render('ajoutGrappe.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }
}
