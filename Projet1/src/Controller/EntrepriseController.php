<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use App\Repository\EntrepriseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class EntrepriseController extends AbstractController
{
    /**
     * @Route("/Entreprise", name="Entreprise")
     */
    public function index(): Response
    {
        return $this->render('Entreprise/index.html.twig', [
            'controller_name' => 'EntrepriseController',
        ]);
    }
    /**
     * @param EntrepriseRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/AfficheRec",name="afficheEntreprise")
     */
    public function Affiche(EntrepriseRepository $repository)
    {
        $avis=$this->getDoctrine()->getManager()->getRepository(avis::class)->findAll();
        $Entreprise=$repository->findAll();
        return $this->render('afficheEntreprise.html.twig',
            ['Entreprise'=>$Entreprise,'avis'=>$avis]);
    }

    /**
     * @Route("/SupprimerEntreprise/{id}",name="deleteEntreprise")
     */
    function Delete($id,EntrepriseRepository $repository)
    {
        $Entreprise=$repository->find($id);
        $em=$this->getDoctrine()->getManager();
        $em->remove($Entreprise);
        $em->flush();
        return $this->redirectToRoute('afficheEntreprise');
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/ajouterEntreprise",name="ajouterEntreprise")
     */
    function Add(Request $request)
    {
        $Entreprise=new Entreprise();
        $form=$this->createForm(EntrepriseType::class, $Entreprise);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($Entreprise);
            $em->flush();
            return $this->redirectToRoute('base');
        }
        return $this->render('ajouterEntreprise.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }


    /**
     * @param Request $request
     * @Route("/ModifierEntreprise/{id}",name="modifierEntreprise")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    function modifier(EntrepriseRepository $repository,$id,Request $request)
    {
        $Entreprise=$repository->find($id);
        $form=$this->createForm(EntrepriseType::class,$Entreprise);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('afficheEntreprise');
        }
        return $this->render('ajoutEntreprise.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }
}
