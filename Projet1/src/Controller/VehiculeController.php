<?php

namespace App\Controller;

use App\Entity\vehicule;
use App\Form\VehiculeType;
use App\Repository\VehiculeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class VehiculeController extends AbstractController
{
    /**
     * @Route("/Vehicule", name="Vehicule")
     */
    public function index(): Response
    {
        return $this->render('Vehicule/index.html.twig', [
            'controller_name' => 'VehiculeController',
        ]);
    }
    /**
     * @param VehiculeRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/AfficheRec",name="afficheVehicule")
     */
    public function Affiche(VehiculeRepository $repository)
    {
        $avis=$this->getDoctrine()->getManager()->getRepository(avis::class)->findAll();
        $Vehicule=$repository->findAll();
        return $this->render('afficheVehicule.html.twig',
            ['Vehicule'=>$Vehicule,'avis'=>$avis]);
    }

    /**
     * @Route("/SupprimerVehicule/{id}",name="deleteVehicule")
     */
    function Delete($id,VehiculeRepository $repository)
    {
        $Vehicule=$repository->find($id);
        $em=$this->getDoctrine()->getManager();
        $em->remove($Vehicule);
        $em->flush();
        return $this->redirectToRoute('afficheVehicule');
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route ("/ajouterVehicule",name="ajouterVehicule")
     */
    function Add(Request $request)
    {
        $Vehicule=new Vehicule();
        $form=$this->createForm(VehiculeType::class, $Vehicule);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->persist($Vehicule);
            $em->flush();
            return $this->redirectToRoute('base');
        }
        return $this->render('ajouterVehicule.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }


    /**
     * @param Request $request
     * @Route("/ModifierVehicule/{id}",name="modifierVehicule")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    function modifier(VehiculeRepository $repository,$id,Request $request)
    {
        $Vehicule=$repository->find($id);
        $form=$this->createForm(VehiculeType::class,$Vehicule);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em=$this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('afficheVehicule');
        }
        return $this->render('ajoutVehicule.html.twig',
            [
                'form'=>$form->createView(),
            ]
        );
    }
}
