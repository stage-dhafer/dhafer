<?php

namespace App\Entity;

use App\Repository\ConducteurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConducteurRepository::class)
 */
class conducteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $grappe_id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $mail_conducteur;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_prenom_conducteur;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $fonction_conducteur;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $status_conducteur;

    /**
     * @ORM\ManyToOne(targetEntity=Grappe::class, inversedBy="category")
     */
    private $grappe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrappeId(): ?int
    {
        return $this->grappe_id;
    }

    public function setGrappeId(int $grappe_id): self
    {
        $this->grappe_id = $grappe_id;

        return $this;
    }

    public function getMailConducteur(): ?string
    {
        return $this->mail_conducteur;
    }

    public function setMailConducteur(string $mail_conducteur): self
    {
        $this->mail_conducteur = $mail_conducteur;

        return $this;
    }

    public function getNomPrenomConducteur(): ?string
    {
        return $this->nom_prenom_conducteur;
    }

    public function setNomPrenomConducteur(string $nom_prenom_conducteur): self
    {
        $this->nom_prenom_conducteur = $nom_prenom_conducteur;

        return $this;
    }

    public function getFonctionConducteur(): ?string
    {
        return $this->fonction_conducteur;
    }

    public function setFonctionConducteur(string $fonction_conducteur): self
    {
        $this->fonction_conducteur = $fonction_conducteur;

        return $this;
    }

    public function getStatusConducteur(): ?string
    {
        return $this->status_conducteur;
    }

    public function setStatusConducteur(string $status_conducteur): self
    {
        $this->status_conducteur = $status_conducteur;

        return $this;
    }

    public function getGrappe(): ?grappe
    {
        return $this->grappe;
    }

    public function setGrappe(?grappe $grappe): self
    {
        $this->grappe = $grappe;

        return $this;
    }
}
