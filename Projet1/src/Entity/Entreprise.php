<?php

namespace App\Entity;

use App\Repository\EntrepriseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntrepriseRepository::class)
 */
class Entreprise
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_entreprise;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_prenom_admin;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $mail_admin;

    /**
     * @ORM\OneToMany(targetEntity=grappe::class, mappedBy="entreprise")
     */
    private $category;

    public function __construct()
    {
        $this->category = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEntreprise(): ?string
    {
        return $this->nom_entreprise;
    }

    public function setNomEntreprise(string $nom_entreprise): self
    {
        $this->nom_entreprise = $nom_entreprise;

        return $this;
    }

    public function getNomPrenomAdmin(): ?string
    {
        return $this->nom_prenom_admin;
    }

    public function setNomPrenomAdmin(string $nom_prenom_admin): self
    {
        $this->nom_prenom_admin = $nom_prenom_admin;

        return $this;
    }

    public function getMailAdmin(): ?string
    {
        return $this->mail_admin;
    }

    public function setMailAdmin(string $mail_admin): self
    {
        $this->mail_admin = $mail_admin;

        return $this;
    }

    /**
     * @return Collection|grappe[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(grappe $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->setEntreprise($this);
        }

        return $this;
    }

    public function removeCategory(grappe $category): self
    {
        if ($this->category->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getEntreprise() === $this) {
                $category->setEntreprise(null);
            }
        }

        return $this;
    }
}
