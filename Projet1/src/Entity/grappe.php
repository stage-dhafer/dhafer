<?php

namespace App\Entity;

use App\Repository\GrappeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GrappeRepository::class)
 */
class grappe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $entreprise_id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom_grappe;

    /**
     * @ORM\ManyToOne(targetEntity=Entreprise::class, inversedBy="category")
     */
    private $entreprise;

    /**
     * @ORM\OneToMany(targetEntity=conducteur::class, mappedBy="grappe")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity=vehicule::class, inversedBy="grappes")
     */
    private $category2;

    public function __construct()
    {
        $this->category = new ArrayCollection();
        $this->category2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntrepriseId(): ?int
    {
        return $this->entreprise_id;
    }

    public function setEntrepriseId(int $entreprise_id): self
    {
        $this->entreprise_id = $entreprise_id;

        return $this;
    }

    public function getNomGrappe(): ?string
    {
        return $this->nom_grappe;
    }

    public function setNomGrappe(string $nom_grappe): self
    {
        $this->nom_grappe = $nom_grappe;

        return $this;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return Collection|conducteur[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(conducteur $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->setGrappe($this);
        }

        return $this;
    }

    public function removeCategory(conducteur $category): self
    {
        if ($this->category->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getGrappe() === $this) {
                $category->setGrappe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|vehicule[]
     */
    public function getCategory2(): Collection
    {
        return $this->category2;
    }

    public function addCategory2(vehicule $category2): self
    {
        if (!$this->category2->contains($category2)) {
            $this->category2[] = $category2;
        }

        return $this;
    }

    public function removeCategory2(vehicule $category2): self
    {
        $this->category2->removeElement($category2);

        return $this;
    }
}
