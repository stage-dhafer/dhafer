<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $grappe;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $immatriculation_vehicule;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $motorisation_vehicule;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $categorie_vehicule;

    /**
     * @ORM\Column(type="date")
     */
    private $date_activation_vehicule;

    /**
     * @ORM\Column(type="date")
     */
    private $date_desactivation_vehicule;

    /**
     * @ORM\ManyToMany(targetEntity=Grappe::class, mappedBy="category2")
     */
    private $grappes;

    public function __construct()
    {
        $this->grappes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrappe(): ?string
    {
        return $this->grappe;
    }

    public function setGrappe(string $grappe): self
    {
        $this->grappe = $grappe;

        return $this;
    }

    public function getImmatriculationVehicule(): ?string
    {
        return $this->immatriculation_vehicule;
    }

    public function setImmatriculationVehicule(string $immatriculation_vehicule): self
    {
        $this->immatriculation_vehicule = $immatriculation_vehicule;

        return $this;
    }

    public function getMotorisationVehicule(): ?string
    {
        return $this->motorisation_vehicule;
    }

    public function setMotorisationVehicule(string $motorisation_vehicule): self
    {
        $this->motorisation_vehicule = $motorisation_vehicule;

        return $this;
    }

    public function getCategorieVehicule(): ?string
    {
        return $this->categorie_vehicule;
    }

    public function setCategorieVehicule(string $categorie_vehicule): self
    {
        $this->categorie_vehicule = $categorie_vehicule;

        return $this;
    }

    public function getDateActivationVehicule(): ?\DateTimeInterface
    {
        return $this->date_activation_vehicule;
    }

    public function setDateActivationVehicule(\DateTimeInterface $date_activation_vehicule): self
    {
        $this->date_activation_vehicule = $date_activation_vehicule;

        return $this;
    }

    public function getDateDesactivationVehicule(): ?\DateTimeInterface
    {
        return $this->date_desactivation_vehicule;
    }

    public function setDateDesactivationVehicule(\DateTimeInterface $date_desactivation_vehicule): self
    {
        $this->date_desactivation_vehicule = $date_desactivation_vehicule;

        return $this;
    }

    /**
     * @return Collection|grappe[]
     */
    public function getGrappes(): Collection
    {
        return $this->grappes;
    }

    public function addGrappe(grappe $grappe): self
    {
        if (!$this->grappes->contains($grappe)) {
            $this->grappes[] = $grappe;
            $grappe->addCategory2($this);
        }

        return $this;
    }

    public function removeGrappe(grappe $grappe): self
    {
        if ($this->grappes->removeElement($grappe)) {
            $grappe->removeCategory2($this);
        }

        return $this;
    }
}
