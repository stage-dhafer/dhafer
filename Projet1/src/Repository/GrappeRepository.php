<?php

namespace App\Repository;

use App\Entity\grappe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method grappe|null find($id, $lockMode = null, $lockVersion = null)
 * @method grappe|null findOneBy(array $criteria, array $orderBy = null)
 * @method grappe[]    findAll()
 * @method grappe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrappeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, grappe::class);
    }

    // /**
    //  * @return Grappe[] Returns an array of Grappe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Grappe
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
