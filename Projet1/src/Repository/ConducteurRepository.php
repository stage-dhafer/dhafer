<?php

namespace App\Repository;

use App\Entity\conducteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method conducteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method conducteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method conducteur[]    findAll()
 * @method conducteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConducteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, conducteur::class);
    }

    // /**
    //  * @return Conducteur[] Returns an array of Conducteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conducteur
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
