<?php

namespace App\Form;

use App\Entity\conducteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConducteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grappe_id')
            ->add('mail_conducteur')
            ->add('nom_prenom_conducteur')
            ->add('fonction_conducteur')
            ->add('status_conducteur')

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => conducteur::class,
        ]);
    }
}
