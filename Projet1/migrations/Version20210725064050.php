<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210725064050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE grappe_vehicule (grappe_id INT NOT NULL, vehicule_id INT NOT NULL, INDEX IDX_DAC88FCD7AB7DF62 (grappe_id), INDEX IDX_DAC88FCD4A4A3511 (vehicule_id), PRIMARY KEY(grappe_id, vehicule_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE grappe_vehicule ADD CONSTRAINT FK_DAC88FCD7AB7DF62 FOREIGN KEY (grappe_id) REFERENCES grappe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE grappe_vehicule ADD CONSTRAINT FK_DAC88FCD4A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conducteur ADD CONSTRAINT FK_236771437AB7DF62 FOREIGN KEY (grappe_id) REFERENCES grappe (id)');
        $this->addSql('CREATE INDEX IDX_236771437AB7DF62 ON conducteur (grappe_id)');
        $this->addSql('ALTER TABLE grappe ADD CONSTRAINT FK_E2AD39F9A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('CREATE INDEX IDX_E2AD39F9A4AEAFEA ON grappe (entreprise_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE grappe_vehicule');
        $this->addSql('ALTER TABLE conducteur DROP FOREIGN KEY FK_236771437AB7DF62');
        $this->addSql('DROP INDEX IDX_236771437AB7DF62 ON conducteur');
        $this->addSql('ALTER TABLE grappe DROP FOREIGN KEY FK_E2AD39F9A4AEAFEA');
        $this->addSql('DROP INDEX IDX_E2AD39F9A4AEAFEA ON grappe');
    }
}
